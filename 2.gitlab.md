# GitLab CI/CD
https://docs.gitlab.com/ee/ci/

# GitLab Runner
## install gitlab runner
sudo curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash  
sudo apt install -y gitlab-runner  
sudo usermod -aG docker gitlab-runner  
sudo gitlab-runner status

# LAB 1 - With Docker
## add gitlab-runner shell
sudo gitlab-runner register  
sudo gitlab-runner list  
sudo gitlab-runner verify

## index.html
```
GitLab CI/CD with Docker
```

## Dockerfile
```
FROM [image-name]
ADD . .
```

## docker-compose.yml
```
version: "3"
services:
	web:
		build: .
		ports:
			- "80:80"
```

## test docker compose
docker-compose up -d --build --force-recreate

## matikan docker composenya
docker-compose down

## add .gitlab-ci.yml
```
stages:
  - test
  - deploy

test_build:
  stage: test
  script: 
    - docker build -t test_build .
  tags: 
    - runner-lab1
    
deploy_docker:
  stage: deploy
  script:
    - docker-compose up -d --build --force-recreate
  only:
    - master
  tags:
    - [tag-name]
```

# LAB 2 - With Heroku
## create gitlab repos


## add gitlab-runner docker
sudo gitlab-runner register  
sudo gitlab-runner list  
sudo gitlab-runner verify

## craete directory
mkdir python-heroku  
cd python-heroku

## create app.py
```
import time
import redis
import os
from flask import Flask

app = Flask(__name__)
cache = redis.from_url(os.environ.get("REDIS_URL"))

def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

@app.route('/')
def hello():
    count = get_hit_count()
    return 'Hello World! Saya sudah terlihat {} kali.'.format(count)

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
```

## app.test.py
```
import unittest
from app import app

class BasicTestCase(unittest.TestCase):
    def test_home(self):
        tester = app.test_client(self)
        response = tester.get('/', content_type='html/text')
        self.assertEqual(response.status_code, 200)

if __name__ ==  '__main__':
    unittest.main()
```

## create Procfile
```
web: gunicorn app:app
```

## create requirements.txt
```
Click==7.0
Flask==1.1.1
gunicorn==20.0.4
itsdangerous==1.1.0
Jinja2==2.10.3
MarkupSafe==1.1.1
redis==3.3.11
Werkzeug==0.16.0
```

## create heroku app

## active heroku redis on app
Resources -> Search "Heroku Redis"  
Plan -> Free  
Provision

## copy heroku api key
Account settings -> API Key -> Reveal  
Save it

## create .gitlab-ci.yml
```
stages:
  - test
  - deploy

test_flask:
  variables:
    REDIS_URL: redis://redis:6379
  services:
    - redis
  image: python:3.7-alpine
  stage: test
  tags:
    - docker
  script:
    - pip install -r requirements.txt
    - python app.test.py

deploy_heroku:
  image: kudaliar032/dpl
  stage: deploy
  only:
    - master
  tags:
    - docker
  script:
    - dpl --provider=heroku --app=${APP_NAME} --api-key=${HEROKU_API}
  environment:
    name: heroku
    url: https://${APP_NAME}.herokuapp.com/
```

## add variables
Settings -> CI / CD -> Variables  
APP_NAME = app name from heroku  
HEROKU_API = Heroku API Key  
Save

## init git
git init

## add gitlab remote repos
git remote add origin [repos link]

## add to stash
git add .

## commit
git commit -m "initial commit :indonesia:"

## push to branch master
git push origin master



